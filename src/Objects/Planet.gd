extends KinematicBody2D

export (int) var astral_body_mass

var number_of_slots = 12
var slots  # slot 0 is in direction Vector(1,0)
var last_used_slot

func _ready() -> void:
    slots = []
    for _i in range(number_of_slots):
        slots.append(0)

func get_astral_body_mass() -> float:
    return astral_body_mass

func get_free_slot(other) -> Vector2:
    # get slot nearest to other
    var angle_to_other = Vector2(-1,0).angle_to(position - other.position)
    var slot = round(angle_to_other/PI * number_of_slots/2)
    # find free slot
    for i in range(int(number_of_slots/2)):
        if slots[slot+i] == 0:
            slot += i
            break 
        elif slots[slot-i] == 0:
            slot -= i
            break
    # block slot
    last_used_slot = slot
    #slots[slot] = 1
    return position + Vector2($CollisionShape2D.shape.radius*1.1,0).rotated(slot/number_of_slots*2 * PI)
