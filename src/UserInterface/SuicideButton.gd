extends Button

func _on_button_up() -> void:
    print(get_path())
    get_node("../../../../../PlayerShip").get_children()[0].disintegrate()
