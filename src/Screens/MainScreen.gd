extends Control

onready var Highscores: VBoxContainer = get_node("Highscores")

func _ready() -> void:
    for i in range(1,10):
        Highscores.get_child(i).text = "" + str(PlayerData.highscores["Map"+str(i)])
