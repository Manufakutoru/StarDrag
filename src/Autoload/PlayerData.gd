extends Node

signal player_score_update
signal player_disintegrated
signal player_died
signal player_fuel_update

var player_score: int = 0 setget set_player_score
var player_disintegrations: int = 0 setget set_player_disintegrations
var player_max_disintegrations: int = 3
var player_fuel: float = 100 setget set_player_fuel

var highscores_file: = File.new()
var highscores_file_path: = "user://highscores.save"
var highscores: = {"Map1": 0, "Map2": 0, "Map3": 0, "Map4": 0, "Map5": 0, 
                   "Map6": 0, "Map7": 0, "Map8": 0, "Map9": 0}

func _ready() -> void:
    if not highscores_file.file_exists(highscores_file_path):
        write_highscores()
    else:
        read_highscores()

func set_player_score(value: int) -> void:
    player_score = value
    player_disintegrations = 0
    emit_signal("player_score_update")

func set_player_disintegrations(value: int) -> void:
    player_disintegrations = value
    emit_signal("player_disintegrated")
    if player_disintegrations >= player_max_disintegrations:
        emit_signal("player_died")

func set_player_fuel(value: float) -> void:
    player_fuel = value
    emit_signal("player_fuel_update")

func reset_player_data() -> void:
    self.player_score = 0

func write_highscores() -> void:
    highscores_file.open(highscores_file_path, File.WRITE)
    highscores_file.store_var(highscores)
    highscores_file.close()

func read_highscores() -> void:
    highscores_file.open(highscores_file_path, File.READ)
    highscores = highscores_file.get_var()
    highscores_file.close()
