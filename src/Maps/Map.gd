extends Node

export (PackedScene) var ShipScene
export (PackedScene) var OtherShipScene
var ship
var start_group
var target_group
var start_body_index = -1
var target_body_index
var target_body_order
var flight_record = []  # [[pos1,rot1,thrust1], ..., [posn,rotn,thrustn]]

func _ready() -> void:
    randomize()
    get_tree().paused = false
    start_group = get_tree().get_nodes_in_group("start")
    target_group = get_tree().get_nodes_in_group("target")
    PlayerData.connect("player_died", self, "_on_PlayerData_player_died")
    # get orders for targets
    target_body_index = []
    for _i in range(len(start_group)):
        target_body_index.append(0)
    target_body_order = []
    for _i in range(len(start_group)): 
        var order = []
        for _ii in range(len(target_group)):
            var rand_int = randi() % len(target_group)
            while rand_int in order:
                rand_int = randi() % len(target_group)
            order.append(rand_int)
        target_body_order.append(order)
    # start game
    start_round()
    
func start_round() -> void:
    start_body_index = (start_body_index + 1) % len(start_group)
    # select start body
    var start_body = start_group[start_body_index]
    # select target body unequal to start body
    var target_body = target_group[target_body_order[start_body_index][target_body_index[start_body_index]]]
    while len(target_group)>0 and start_body == target_body:
        target_body_index[start_body_index] = (target_body_index[start_body_index] + 1) % len(target_group)
        target_body = target_group[target_body_order[start_body_index][target_body_index[start_body_index]]]
    # move flag
    $TargetFlag.set_position(target_body.position +
                             target_body.get_node("CollisionShape2D").shape.radius * 0.9 * Vector2(1,-1))
    # instantiate ship
    ship = ShipScene.instance()
    # pass start and target to active ship
    ship.start_body = start_body
    ship.target_body = target_body
    # pass start position put ship in scene tree
    var ship_pos = start_body.get_free_slot(target_body)
    $PlayerShip.call_deferred("add_child", ship)
    ship.connect('target_reached', self, '_on_ActiveShip_target_reached')
    ship.connect('disintegrated', self, '_on_ActiveShip_disintegrated')
    ship.call_deferred("set_to_start_position", 
                       ship_pos, 
                       Vector2(1,0).angle_to(ship_pos - start_body.position), 
                       $AstralBodies.get_children())
    yield(get_tree().create_timer(1.5), "timeout")
    $FlightRecorderTimer.set_wait_time(0.1)
    $FlightRecorderTimer.start()
    ship.activate()
    # start other ships
    for other_ship in $OtherShips.get_children():
        other_ship.start()

func _on_ActiveShip_target_reached():
    # stop recording
    $FlightRecorderTimer.stop()
    # calculate points and remove player ship
    ship.queue_free()
    # reset other ships
    for other_ship in $OtherShips.get_children():
        other_ship.reset()
    # add other ship and pass flight record
    if len(flight_record) > 0:
        var other_ship = OtherShipScene.instance()
        other_ship.set_flight_record(flight_record)
        other_ship.reset()
        $OtherShips.call_deferred("add_child", other_ship)
    # clear flight_record
    flight_record = []
    # move index forward
    target_body_index[start_body_index] = (target_body_index[start_body_index] + 1) % len(target_group)
    # restart player ship
    start_round()

func _on_ActiveShip_disintegrated():
    $FlightRecorderTimer.stop()
    ship.queue_free()
    flight_record = []
    for other_ship in $OtherShips.get_children():
        other_ship.reset()
    start_round()

func _on_FlightRecorderTimer_timeout():
    flight_record.append([ship.get_position(), ship.get_rotation(), ship.thrust != Vector2(0,0)])

func _on_PlayerData_player_died() -> void:
    var scene_name: = get_tree().get_current_scene().get_name()
    if PlayerData.player_score > PlayerData.highscores[scene_name]:
        PlayerData.highscores[scene_name] = PlayerData.player_score
        PlayerData.write_highscores()
