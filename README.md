# StarDrag

Fly your ship safe and set the routes for ships to come.

This was created as a submission for the 202012 Edition of [LibreJam](https://leagueh.xyz/en/jam.html) with the theme "Engineering in Space".

## Overview

The automatic star ship route planning system broke down. You are the leading engineer and need to control the stellar fleet manually now! 

## How to Play

Use W, A, D to fly your ship from the start planet to the target planet marked by the flag. 
Score is based on fuel left when arriving at the target. You get 3 tries between successful flights.

## Licensing

Code is under GPLv3.

The handdrawn assets were made by [vivizlorbeeren](https://www.instagram.com/vivizlorbeeren/) and are under the CC-BY-SA 4.0.

The Font "Funtype" used in this Project is licensed under the OFL and
obtained from [fontlibrary](https://fontlibrary.org/en/font/funtype).
